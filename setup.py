from setuptools import setup, find_packages

setup(name                = 'ailever',
      version             = '1.0.120',
      description         = 'Clever Artificial Intelligence',
      author              = 'ailever',
      author_email        = 'gitlab@aileverlabs.com',
      url                 = 'https://gitlab.com/aileverlabs/ailever',
      install_requires    = ['dash', 'dash_bootstrap_components', 'plotly', 'beautifulsoup4', 'yahooquery', 'finance-datareader', 'statsmodels', 'seaborn', 'matplotlib', 'pandas', 'numpy', 'requests'],
      packages            = find_packages(exclude = []),
      keywords            = ['ailever', 'clever', 'artificial intelligence'],
      python_requires     = '>=3',
      package_data        = {},
      zip_safe            = False,
      classifiers         = ['Programming Language :: Python :: 3.6',
                             'Programming Language :: Python :: 3.7',
                             'Programming Language :: Python :: 3.8',
                             'Programming Language :: Python :: 3.9',
                             'Programming Language :: Python :: 3.10'])
