### reduce
```python
from functools import reduce
reduce(lambda x, y: x + y, [1,2,3], 0) # (((0 + 1) + 2) + 3)
```

### partial
`Partially, function transformation under fixed (kward) arguments`
```python
from functools import partial

def power(base, exponent):
    return base ** exponent

square = partial(power, exponent=2)
cube = partial(power, exponent=3)

square(2) # 4
cube(2)   # 8

cube.func is power
cube.args 
cube.keywords
```

### update_wrapper
```python
from functools import update_wrapper
# update_wrapper's arguments : wrapper, wrapped, assigned=('__module__', '__name__', '__qualname__', '__doc__', '__annotations__'), updated=('__dict__',)

def wrapper(*args, **kwargs): 
    pass

def add(a, b):
    """ ADD a + b """
    return a + b

print(wrapper.__doc__) # None
print(wrapper.__name__) # wrapper

update_wrapper(wrapper=wrapper, wrapped=add)

print(wrapper.__doc__) # None → ADD a + b
print(wrapper.__name__) # wrapper → add
```


### wraps
`functools.wraps(func) == partial(update_wrapper, wrapped=func)`
```python
from functools import wraps

```
